/**
 * Rejects requests which have invalid tokens or if user has
 * not logged in
 */

module.exports = (req, res, next) => {
  const user = req.user

  if(user.auth){
    next()
  } else {
    res.send("You are not authorized to visit this page. <a href='/'>Home</a>")
  } 
}
