const auth = require("./auth")
const gate = require("./gate")
const setup = require("./setup")

module.exports = {
  auth,
  gate,
  setup
}
