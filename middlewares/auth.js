/**
 * Verifies JSON Web Token and check for the user in the database
 * Stores authenticity in `user { auth: false }`
 */

const jwt = require("jsonwebtoken")

const verify = async (db, username) => {
  const query = `SELECT username FROM users WHERE username = "${username}"` 

  return new Promise((resolve, reject) => {
    db.get(query, (err, row) => {
      if(err){
        reject(err)
      }

      resolve(row)
    })   
  })
}

module.exports = async (req, res, next) => {
  req.user = {
    auth: false
  }

  const { token } = req.cookies

  if(token){
    try {
      const decoded_token = jwt.verify(req.cookies.token, process.env.JWT_SECRET)

      await verify(req.db, decoded_token.username)

      req.user.auth = true
      req.user.data = decoded_token

      return next()

    } catch(err) { // Invalid token

      console.log(err)
    }
  }

  return next()
}
