/**
 * Add SQLite DB instance in the `request` object
 */

const sqlite3 = require("sqlite3").verbose()

const createTables = () => {
  db.run(`
    CREATE TABLE IF NOT EXISTS users(
      username VARCHAR(20) NOT NULL,
      password VARCHAR(20) NOT NULL,

      PRIMARY KEY (username)
    )
  `)

  db.run(`
    CREATE TABLE IF NOT EXISTS surveys(
      survey_id VARCHAR(220) NOT NULL,
      name VARCHAR(200) NOT NULL,
      owner VARCHAR(20) NOT NULL,

      PRIMARY KEY (survey_id),
      FOREIGN KEY (owner) REFERENCES users(username)
    )
  `)

  db.run(`
    CREATE TABLE IF NOT EXISTS questions(
      question VARCHAR(500) NOT NULL,
      survey_id VARCHAR(220) NOT NULL,
      no INT,
      yes INT,

      PRIMARY KEY (question, survey_id),
      FOREIGN KEY (survey_id) REFERENCES surveys(survey_id)
    )
  `)
}

const db = new sqlite3.Database("toddle.sqlite3", createTables)

module.exports = (req, res, next) => {
  req.db = db

  next()
}
