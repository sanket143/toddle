let app = new Vue({
  el: "#app",
  data: {
    name: "",
    question: "",
    questions: []
  },
  computed: {
    showCreate(){
      if(this.name.length != 0 && this.questions.length != 0){
        return true
      }

      return false
    }
  },
  methods: {
    addQuestion(){
      this.questions.push(this.question)
      this.question = ""
    },
    createSurvey(){
      let survey = {
        name: this.name,
        questions: this.questions
      }

      let query = `mutation CreateSurvey($survey: CreateSurveyInput) {
        createSurvey(survey: $survey)
      }`

      $.ajax({
        type: "POST",
        url: "/graphql",
        contentType: "application/json",
        data: JSON.stringify({
          query,
          variables: {
            survey
          }
        }),
        dataType: "json",
        success(data){
          window.location = "/survey"
        },
        error(err){
          console.log(err)
        }
      })
    }
  }
})
