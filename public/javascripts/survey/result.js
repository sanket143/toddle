let app = new Vue({
  el: "#app",
  data: {
    name: "",
    survey_id: $("#survey_id").val(),
    questions: []
  },
  mounted(){
    const survey_id = this.survey_id

    const query = `query GetSurvey($survey_id: String){
      getSurvey(survey_id: $survey_id){
        name,
        questions {
          question,
          yes,
          no
        }
      }
    }`

    $.ajax({
      type: "POST",
      url: "/graphql",
      contentType: "application/json",
      data: JSON.stringify({
        query,
        variables: {
          survey_id
        }
      }),
      dataType: "json",
      success: (data) => {
        this.name = data.data.getSurvey.name
        this.questions = data.data.getSurvey.questions
      },
      error(err){
        console.log(err)
      }
    })
  }
})
