let app = new Vue({
  el: "#app",
  data: {
    name: "",
    survey_id: $("#survey_id").val(),
    questions: []
  },
  methods: {
    submitSurvey(){
      const survey = {
        survey_id: this.survey_id,
        questions: this.questions
      }

      const query = `mutation SubmitSurvey($survey: SubmitSurveyInput){
        submitSurvey(survey: $survey)
      }`

      $.ajax({
        type: "POST",
        url: "/graphql",
        contentType: "application/json",
        data: JSON.stringify({
          query,
          variables: {
            survey
          }
        }),
        dataType: "json",
        success(data){
          window.location = "/survey/take"
        },
        error(err){
          console.log(err)
        }
      })
    }
  },
  mounted(){
    const survey_id = this.survey_id

    const query = `query GetSurvey($survey_id: String){
      getSurvey(survey_id: $survey_id){
        name,
        questions {
          question
        }
      }
    }`

    $.ajax({
      type: "POST",
      url: "/graphql",
      contentType: "application/json",
      data: JSON.stringify({
        query,
        variables: {
          survey_id
        }
      }),
      dataType: "json",
      success: (data) => {
        this.name = data.data.getSurvey.name
        this.questions = data.data.getSurvey.questions.map((question) => {
          question.val = ""
          return question
        })
      },
      error(err){
        console.log(err)
      }
    })
  }
})
