$(document).ready(function(){
  $("#sign-in").submit((e) => {
    e.preventDefault();

    let username = $("#username").val()
    let password = $("#password").val()
    let user = {
      username,
      password
    }

    let query = `mutation AuthenticateUser($user: UserInput) {
      authenticateUser(user: $user)
    }`

    $.ajax({
      type: "POST",
      url: "/graphql",
      contentType: "application/json",
      data: JSON.stringify({
        query,
        variables: { user }
      }),
      dataType: "json",
      success(data){
        $("#response").text(JSON.stringify(data, null, 2))

        setTimeout(() => {
          window.location = "/survey"
        }, 2000)
      },
      error(err){
        console.log(err)
      }
    })

    return false
  })
})
