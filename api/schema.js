const { buildSchema } = require("graphql")

module.exports = buildSchema(`
  input UserInput {
    username: String!,
    password: String!
  }

  input CreateSurveyInput {
    name: String!,
    questions: [String]
  }

  input QuestionInput {
    question: String!,
    val: String!
  }

  input SubmitSurveyInput {
    survey_id: String!,
    questions: [QuestionInput] 
  }

  type Question {
    question: String!,
    yes: Int,
    no: Int
  }

  type Survey {
    name: String!,
    owner: String!,
    survey_id: String!,
    questions: [Question]
  }

  type Query {
    hello: String
    getSurvey(survey_id: String): Survey
  }

  type Mutation {
    authenticateUser(user: UserInput): String
    createSurvey(survey: CreateSurveyInput): Boolean
    submitSurvey(survey: SubmitSurveyInput): Boolean
  }
`)
