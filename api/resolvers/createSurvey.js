const slugify = (name) => {
  name = name.match(/\w+/g).join(" ")
  name = name.replace(/\s+/g, "-")
  name = name.toLowerCase()

  return name
}

const addSurvey = (db, survey_id, name, username) => {
  db.run(`INSERT OR IGNORE INTO surveys VALUES ("${survey_id}", "${name}", "${username}")`)
}

const addQuestion = (db, survey_id, question) => {
  db.run(`INSERT OR IGNORE INTO questions VALUES("${question}", "${survey_id}", 0, 0)`)
}

module.exports = (payload, { res, user, db }) => {
  if(!user.auth){
    return false
  }

  const { name, questions } = payload.survey
  const { username } = user.data
  const survey_id = slugify(`${name} ${username}`)

  addSurvey(db, survey_id, name, username)

  for(question of questions){
    addQuestion(db, survey_id, question)
  }

  return true 
}
