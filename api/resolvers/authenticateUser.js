const jwt = require("jsonwebtoken")

module.exports = (payload, { res, db }) => {
  const { username, password } = payload.user
  const token = jwt.sign({ username }, process.env.JWT_SECRET);

  res.cookie("token", token, { maxAge: 9_00_000, httpOnly: true })

  db.run(`INSERT OR IGNORE INTO users VALUES ("${username}", "${password}")`)

  return token 
}
