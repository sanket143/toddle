module.exports = (payload, { res, user, db }) => {
  if(!user.auth){
    return false
  }

  const { survey_id, questions } = payload.survey

  console.log(payload)
  questions.forEach((question) => {
    const query = `UPDATE questions
      SET ${question.val} = ${question.val} + 1
      WHERE question = "${question.question}" AND
      survey_id = "${survey_id}"
    `

    db.run(query)
  })
  
  return true
}
