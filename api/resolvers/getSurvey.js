module.exports = async (payload, { query, res, user, db }) => {
  const { survey_id } = payload
  const questions_query = `SELECT * FROM questions WHERE survey_id = "${survey_id}"`
  const survey_query = `SELECT * FROM surveys WHERE survey_id = "${survey_id}"`

  const promise = new Promise((resolve, reject) => {
    db.get(survey_query, (err, row) => {
      if(err){
        return reject(err)
      }

      if(row === undefined){
        return reject()
      }

      let survey = row;

      db.all(questions_query, (err, rows) => {
        if(err){
          return reject(err)
        }

        survey.questions = rows
        resolve(survey)
      })
    })
  })

  try {
    const survey = await promise
    return survey
  } catch(err) {
    console.log(err)
  }
}
