const authenticateUser = require("./authenticateUser")
const createSurvey = require("./createSurvey")
const submitSurvey = require("./submitSurvey")
const getSurvey = require("./getSurvey")

module.exports = {
  authenticateUser,
  createSurvey,
  submitSurvey,
  getSurvey
}
