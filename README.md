# Toddle
Toddle Coding Assessment Challenge

```bash
$ npm install
$ npm start # http://localhost:3000
```

## Tasks

**1. Authentication Endpoint** [[Source]](https://gitlab.com/sanket143/toddle/blob/master/api/resolvers/authenticateUser.js)
- [x] It is a public endpoint.
- [x] Request body will contain an arbitrary username/password pair. Treat it as a mock authentication service and accept any username/password.
- [x] Return a signed JSON Web Token (JWT, https://jwt.io/) which can be used to validate future requests.
- [x] API in GraphQL

**2. Survey REST API endpoints:**
- [x] The JWT obtained in the “Authentication” endpoint will be attached to each request.
- [x] If the JWT is missing or invalid, these endpoints should reject the request.
- [x] Creating a Survey [[Source]](https://gitlab.com/sanket143/toddle/blob/master/api/resolvers/createSurvey.js)
- [x] Taking a Survey [[Source]](https://gitlab.com/sanket143/toddle/blob/master/api/resolvers/submitSurvey.js)
- [x] Getting results of a Survey [[Source]](https://gitlab.com/sanket143/toddle/blob/master/api/resolvers/getSurvey.js)
- [x] API in GraphQL
- [x] Use SQLite to persist data [[Source]](https://gitlab.com/sanket143/toddle/-/blob/master/middlewares/setup.js)
- [ ] No need to implement a frontend

**3. Image Thumbnail generation endpoint:**
- [ ] Request will contain a public image URL. Download the image, resize to 50x50 pixels, and return the resulting thumbnail.