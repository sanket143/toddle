const express = require("express")
const jwt = require("jsonwebtoken")

const router = express.Router()

router.get("/", (req, res, next) => {
  res.render("index", { user: req.user })
})

router.get("/logout", (req, res) => {
  res.clearCookie("token")
  res.send("Successfully logged out")
})

module.exports = router;
