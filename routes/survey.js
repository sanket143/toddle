const express = require("express")
const router = express.Router()

router.get("/", (req, res) => {
  res.render("survey", { user: req.user })
})

/** Create Survey */
router.get("/create", (req, res) => {
  res.render("survey/create", { user: req.user })
})

/** Take surveys */
router.get("/take", async (req, res) => {
  const query = "SELECT * FROM surveys"

  req.db.all(query, (err, rows) => {
    if(err){
      return console.log(err)
    }

    res.render("survey/list", { action: "take", user: req.user, surveys: rows })
  })
})

router.get("/take/:id", async (req, res) => {
  const survey_id = req.params.id

  res.render("survey/take", { user: req.user, survey_id })
})

/** Show Survey Result */
router.get("/result", async (req, res) => {
  const query = "SELECT * FROM surveys"

  req.db.all(query, (err, rows) => {
    if(err){
      return console.log(err)
    }

    res.render("survey/list", { action: "result", user: req.user, surveys: rows })
  })
})

router.get("/result/:id", async (req, res) => {
  const survey_id = req.params.id

  res.render("survey/result", { user: req.user, survey_id })
})

module.exports = router;
